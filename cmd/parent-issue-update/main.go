package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/xanzy/go-gitlab"
)

const (
	defaultIssuesPerPage = 200
	defaultSyncDays      = 1
	defaultMaxLevels     = 0
)

type env struct {
	ref          string
	token        string
	repoPath     string
	pid          string
	branch       string
	syncDays     int
	maxLevels    int
	addChangelog bool
	dryRun       bool
	updateClosed bool
}

type service struct {
	ctx    context.Context
	client *gitlab.Client
	env    *env
	wg     sync.WaitGroup
}

func flagToBool(s string) bool {
	s = strings.ToLower(s)
	return s == "1" || s == "true" || s == "y" || s == "yes"
}

func environment() *env {
	// docs https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
	ref := os.Getenv("PIU_REF")

	e := &env{
		ref:          ref,
		repoPath:     os.Getenv("PIU_REPO"),
		pid:          os.Getenv("PIU_PROJECT_ID"),
		branch:       branch(ref),
		token:        os.Getenv("PIU_TOKEN"),
		dryRun:       flagToBool(os.Getenv("PIU_DRY_RUN")),
		addChangelog: flagToBool(os.Getenv("PIU_ADD_CHANGELOG")),
		updateClosed: flagToBool(os.Getenv("PIU_UPDATE_CLOSED")),
	}

	var err error

	syncDays := os.Getenv("PIU_SYNC_DAYS")
	e.syncDays, err = strconv.Atoi(syncDays)
	if err != nil {
		if strings.ToLower(syncDays) == "all" {
			e.syncDays = -1
		} else {
			e.syncDays = defaultSyncDays
		}
	}

	e.maxLevels, err = strconv.Atoi(os.Getenv("PIU_MAX_LEVELS"))
	if err != nil {
		e.maxLevels = defaultMaxLevels
	}

	return e
}

func branch(ref string) string {
	parts := strings.Split(ref, "/")
	skip := map[string]bool{"refs": true, "tags": true, "heads": true, "remotes": true}
	i := 0

	for i = 0; i < len(parts); i++ {
		if _, ok := skip[parts[i]]; !ok {
			break
		}
	}

	return strings.Join(parts[i:], "/")
}

func (e *env) debugPrint() {
	log.Printf("Repo: %v", e.repoPath)
	log.Printf("Ref: %v", e.ref)
	log.Printf("Project ID: %v", e.pid)
	log.Printf("Sync days: %v", e.syncDays)
	log.Printf("Max levels: %v", e.maxLevels)
	log.Printf("Dry run: %v", e.dryRun)
	log.Printf("Add comments: %v", e.addChangelog)
	log.Printf("Update closed: %v", e.updateClosed)
	log.Printf("Dry run: %v", e.dryRun)
	log.Printf("Token length: %v", len(e.token))
}

func (s *service) fetchGitlabIssues() ([]*gitlab.Issue, error) {
	var allIssues []*gitlab.Issue

	state := "all"

	opt := &gitlab.ListProjectIssuesOptions{
		State:       &state,
		ListOptions: gitlab.ListOptions{PerPage: defaultIssuesPerPage},
	}

	for {
		issues, resp, err := s.client.Issues.ListProjectIssues(s.env.pid, opt)
		if err != nil {
			return nil, err
		}

		allIssues = append(allIssues, issues...)

		if resp.NextPage == 0 {
			break
		}

		opt.Page = resp.NextPage
	}
	log.Printf("Fetched gitlab todo issues. count=%v", len(allIssues))

	return allIssues, nil
}

func (s *service) fetchIssuesByID(issues []int) ([]*gitlab.Issue, error) {
	log.Printf("Fetching issues by ID. count=%v", len(issues))
	var wg sync.WaitGroup

	var allIssues []*gitlab.Issue
	for _, i := range issues {
		wg.Add(1)
		go func(id int) {
			defer wg.Done()

			issue, _, err := s.client.Issues.GetIssue(s.env.pid, id)
			if err != nil {
				log.Printf("Failed to retrieve an issue. issue=%v err=%v", id, err)
				return
			}

			allIssues = append(allIssues, issue)
		}(i)
	}

	log.Printf("Waiting for issues to be fetched by ID...")
	wg.Wait()

	return allIssues, nil
}

func createComment(changelog []string) string {
	if len(changelog) == 0 {
		return ""
	}

	var str strings.Builder
	str.WriteString("Issue update changelog:\n")
	for _, s := range changelog {
		str.WriteString(fmt.Sprintf("- %s\n", s))
	}
	return str.String()
}

func (s *service) updateIssue(i *Issue, body string, changelog []string) {
	defer s.wg.Done()

	log.Printf("About to update an issue. issue=%v", i.ID)
	if s.env.dryRun {
		log.Printf("Dry run mode.")
		return
	}

	opt := &gitlab.UpdateIssueOptions{
		Description: &body,
	}
	_, _, err := s.client.Issues.UpdateIssue(s.env.pid, i.ID, opt)

	if err != nil {
		log.Printf("Error while editing an issue. issue=%v err=%v", i.ID, err)
		return
	}

	log.Printf("Updated an issue. issue=%v", i.ID)

	if s.env.addChangelog && len(changelog) > 0 {
		body := createComment(changelog)
		comment := &gitlab.CreateIssueNoteOptions{
			Body: &body,
		}
		_, _, err = s.client.Notes.CreateIssueNote(s.env.pid, i.ID, comment)
		if err != nil {
			log.Printf("Error while adding a comment. issue=%v err=%v", i.ID, err)
			return
		}

		log.Printf("Added a comment to the issue. issue=%v", i.ID)
	}
}

func main() {
	log.SetOutput(os.Stdout)
	env := environment()

	ctx := context.Background()
	git, err := gitlab.NewClient(env.token)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	svc := &service{
		ctx:    ctx,
		client: git,
		env:    env,
	}

	env.debugPrint()

	ghIssues, err := svc.fetchGitlabIssues()
	if err != nil {
		log.Panic(err)
	}

	if len(ghIssues) == 0 {
		log.Printf("There are no issues to update")
		return
	}

	tr := NewTree(ghIssues)
	missing, err := svc.fetchIssuesByID(tr.missing)
	if err != nil {
		log.Panic(err)
	}
	tr.AddParentIssues(missing)
	issues := tr.Issues()

	e := &Editor{
		MaxLevels: svc.env.maxLevels,
	}

	for _, i := range issues {
		canProcess := i.IsOpened() || (i.IsClosed() && svc.env.updateClosed)
		if !canProcess {
			log.Printf("Skipping issue update. issue=%v status=%v", i.ID, i.Status)
			continue
		}

		body, changeLog, err := e.Update(i, true /*add missing*/)
		if err != nil {
			log.Printf("Failed to update issue body. issue=%v err=%v", i.ID, err)
			continue
		}

		if body == i.Body {
			log.Printf("Skipping identical issue body. issue=%v", i.ID)
			continue
		}

		svc.wg.Add(1)
		go svc.updateIssue(i, body, changeLog)
	}

	log.Printf("Waiting for issue update to finish...")
	svc.wg.Wait()

	// help logger to flush
	time.Sleep(1 * time.Second)
}
