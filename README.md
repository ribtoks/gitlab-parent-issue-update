# gitlab-parent-issue-update

A mirror of https://github.com/ribtoks/parent-issue-update for GitLab CI

Application that updates parent issues that are linked by child issues. When child issue is closed or opened, referenced parent is updated. You can use this to create Epics using GitHub Issues or simply for better tracking of dependent issues. Issue hierarchies are supported too.

In order to link child issues to parent issues, add a line Parent: #1234 or Epic: #1234 or Parent issue: #1234 anywhere in the child issue body.

## Screenshot

![screenshot](screenshot.png)

## Usage

In order to use this app, you need to get the binary into your CI pipeline. You can do it using `go get` command or you can prebuild it and download ready binary using `curl` or `wget`. `gitlab-tdg` reads certain environment variables (inherited from [parent-issue-update](https://github.com/ribtoks/parent-issue-update/)) instead of command-line parameters.

Also you need to create a PAT (personal access token) in your GitLab user's settings page -> Access Tokens with access to the API and then in repository settings CI/CD Variables section add it as a variable (don't forget to protect and mask it).

## Basic example

You need to define some environmental variables for `parent-issue-update` to pick up:

```
variables:
  PIU_REPO: "${CI_PROJECT_PATH}"
  PIU_REF: "${CI_COMMIT_REF_NAME}"
  PIU_TOKEN: "${MY_TEST_PAT}"
  PIU_PROJECT_ID: "${CI_PROJECT_ID}"
```

You need to create a PAT with access to the API and provide it through CI/CD variables (`MY_TEST_PAT`) in the example above.

## All parameters

Variable | Meaning
--- | ---
`PIU_REPO` | Repository name in the format of `owner/repo` (required)
`PIU_REF` | Git ref: branch or pull request (required)
`PIU_SHA` | SHA-1 value of the commit
`PIU_TOKEN` | GitLab token used to create or close issues (required) 
`PIU_PROJECT_ID` | GitLab project ID (required)
`PIU_UPDATE_CLOSED` | Whether to update issues that are already closed
`PIU_ADD_CHANGELOG` | Add a comment with a summary of changes

