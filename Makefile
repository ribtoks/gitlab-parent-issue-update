build:
	export GOFLAGS="-mod=vendor"
	go build -o bin/parent-issue-update cmd/parent-issue-update/*

test:
	go test ./...

vendors:
	go mod tidy
	go mod vendor
